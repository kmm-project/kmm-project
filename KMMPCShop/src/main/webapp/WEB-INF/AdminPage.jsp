<%-- 
    Document   : Adminpage
    Created on : 2020.09.07., 19:41:28
    Author     : Tibee
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        
        <%@ include file="/WEB-INF/navbar.jsp" %>
        <%@include file="/background.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <h1>Add products</h1>
        <form action="AdminPage" method="post">
           
            <label for="name">Product name:</label><br>
            <input type="text" id="name" name="name"><br>
            <label for="manufacturer">Manufacturer:</label><br>
            <input type="text" id="manufacturer" name="manufacturer"><br>
            <label for="description">Description</label><br>
            <input type="text" id="description" name="description"><br>
            <label for="description">Price</label><br>
            <input type="text" id="price" name="price"><br>
            <label for="description">Product Type</label><br>
            <input type="text" id="productType_id" name="productType_id"><br>
            <input type="submit" name="action" value="Submit">
        </form> 
        
     
            <form action="AdminPage" method="get">
           
                <br>
                <br>
                <caption><h2>List of products in stock: </h2></caption>
            <c:forEach items="${listProductsPageBean.productTypes}" var="productType">
            <div align="center">
            <table border="1" cellpadding="5" style="background-color:#FFFFFF">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Manufacturer</th>
                <th scope="col">Price</th>
                
            </tr>
           <c:forEach items="${productType.products}" var="product">

                                        <c:choose>
                                            <c:when test="${listProductsPageBean.selectedProducts.contains(product)}">
                <tr>
                    <th scope="row"><c:out value="${product.id}" /></th>
                    <td><c:out value="${product.name}" /></td>
                    <td><c:out value="${product.description}" /></td>
                    <td><c:out value="${product.manufacturer}" /></td>
                    <td><c:out value="${product.price}" /></td>
                    
                </tr>
                       </c:when>       
                        <c:otherwise>
                         <c:if test="${!listProductsPageBean.filteredProducts.contains(product)}">
                    <tr>
                        <th scope="row"> <c:out value="${product.id}" /></th>
                            <td><c:out value="${product.name}" /></td>
                             <td><c:out value="${product.description}" /></td>
                            <td><c:out value="${product.manufacturer}" /></td>
                            <td><c:out value="${product.price}" /></td>
                                                               
                </tr>
                 </c:if>
                </c:otherwise>
                </c:choose>
            </c:forEach>
                
        </table>
    </div>
                </c:forEach>
<c:forEach items="${listProductsPageBean.filteredProducts}" var="product">

                            <th scope="row">"${product.id}</th>
                            <td><c:out value="${product.name}" /></td>
                             <td><c:out value="${product.description}" /></td>
                            <td><c:out value="${product.manufacturer}" /></td>
                            <td><c:out value="${product.price}" /></td>
        <br>
    </c:forEach>
        <br>
        <br>
            <c:forEach items="${listProductsPageBean.selectedProducts}" var="product">

                            <th scope="row">"${product.id}</th>
                            <td><c:out value="${product.name}" /></td>
                             <td><c:out value="${product.description}" /></td>
                            <td><c:out value="${product.manufacturer}" /></td>
                            <td><c:out value="${product.price}" /></td>
        <br>
    </c:forEach>
            </form>
                
    </body>
</html>
