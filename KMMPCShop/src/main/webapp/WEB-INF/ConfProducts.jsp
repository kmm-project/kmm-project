<%-- 
    Document   : configuration
    Created on : 2020.08.31., 12:57:25
    Author     : MMD
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"
    >
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/background.jsp" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            >
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title>KMMproject</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/navbar.jsp" %>
        <h1>Configuration</h1>

        <c:forEach items="${listProductsPageBean.productTypes}" var="productType">

            <div id="accordion">
                <div class="card">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#${productType.name}">
                            <td>${productType.name}</td>
                        </a>
                    </div>
                    <div id="${productType.name}" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Manufacturer</th>
                                        <th scope="col">Select</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <c:forEach items="${productType.products}" var="product">

                                        <c:choose>
                                            <c:when test="${listProductsPageBean.selectedProducts.contains(product)}">
                                                <tr>
                                                    <th scope="row">${product.id}</th>
                                                    <td>${product.name}</td>
                                                    <td>${product.description}</td>
                                                    <td>${product.manufacturer}</td>
                                                    <td>
                                                        <form action = "ConfProducts" method = "POST">
                                                            <input type = "hidden" id="manipulateFilter" name="manipulateFilter" value=${product.id}>
                                                            <button type="submit" 
                                                                    onclick='manipulateFilter.submit();window.location = "${pageContext.request.contextPath}/ConfProducts"'                                                                             
                                                                    class="btn btn-primary">Diselect</button> 
                                                        </form></td></tr>
                                                    </c:when>

                                                <c:otherwise>
                                                    <c:if test="${!listProductsPageBean.filteredProducts.contains(product)}">

                                                    <tr>
                                                        <th scope="row">${product.id}</th>
                                                        <td>${product.name}</td>
                                                        <td>${product.description}</td>
                                                        <td>${product.manufacturer}</td>
                                                        <td>
                                                            <form action = "ConfProducts" method = "POST">
                                                                <input type = "hidden" id="manipulateFilter" name="manipulateFilter" value=${product.id}>
                                                                <button type="submit" 
                                                                        onclick='manipulateFilter.submit(); http - equiv = "Refresh" content = "5;url=index.jsp"'                                                                             
                                                                        class="btn btn-primary">Select</button> 
                                                            </form></td>
                                                    </tr>

                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>

                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>



            </div>

        </c:forEach>

    <c:forEach items="${listProductsPageBean.filteredProducts}" var="product">

        <th scope="row">${product.id}</th>
        <td>${product.name}</td>
        <td>${product.description}</td>
        <td>${product.manufacturer}</td>
        <br>
    </c:forEach>
        <br>
        <br>
            <c:forEach items="${listProductsPageBean.selectedProducts}" var="product">

        <th scope="row">${product.id}</th>
        <td>${product.name}</td>
        <td>${product.description}</td>
        <td>${product.manufacturer}</td>
        <br>
    </c:forEach>



    <%-- 
<button type="button" 
    onclick='${listProductsPageBean.clearFilteredProducts()}; http - equiv = "Refresh" content = "5;url=index.jsp"'                                            
    class="btn btn-primary">Clear all</button>

    <button type="button" 
            onclick='window.location = "${pageContext.request.contextPath}/ConfProducts"'                                            
            class="btn btn-primary">Save</button>
    --%>
</body>
</html>
