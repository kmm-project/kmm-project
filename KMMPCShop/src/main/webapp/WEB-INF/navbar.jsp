 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta charset="utf-8">  
<meta name="viewport" content="width=device-width, initial-scale=1">  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">  




<nav class="navbar navbar-inverse">  
    <div class="container-fluid">  
        <div class="navbar-header">  
            <a class="navbar-brand" href="">KMMPcShop</a>  
        </div>  
        <ul class="nav navbar-nav flex-row">  
            <li><a href="index.jsp">Home page </a></li>  

            <li><a href="${pageContext.request.contextPath}/ConfProducts">Configuration page </a></li> 
            <li><a href="${pageContext.request.contextPath}/EmailForm">Contact Us </a> </li>
            <li><a href="${pageContext.request.contextPath}/AdminPage">AdminPage </a> </li>
            <li><a href="aboutUs.jsp">About Us </a></li>   
                <c:if test="${sessionScope.loggedIn==true}">
                <li><a href="${pageContext.request.contextPath}/UserSettings.jsp">Settings </a> </li>
                <li><a href="${pageContext.request.contextPath}/">My Configurations </a> </li>
                <li><a href="${pageContext.request.contextPath}/LogoutServlet">Logout </a> </li> 
             </c:if>
                <c:if test="${sessionScope.loggedIn!=true}">
            <li><a href="${pageContext.request.contextPath}/UserRegistry.jsp">Register </a> </li>  
            <li><a href="${pageContext.request.contextPath}/Login.jsp">Login </a> </li> 
            </c:if>
        </ul>  
    </div> 

</nav>  

  
