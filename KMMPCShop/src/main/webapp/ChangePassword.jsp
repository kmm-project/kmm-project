<%-- 
    Document   : ChangePassword
    Created on : 2020.09.10., 14:41:47
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div><%@include file="/background.jsp" %></div>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>  
        <title>Change Password</title>
    </head>
    <body>
        
        <%@ include file="/WEB-INF/navbar.jsp" %>
        <div class="container">
            <form method="POST" action="ChangePassword">

                <div class="form-group row">
                    <label for="oldPassword">Old Password:</label>
                    <input type="password" class="form-control" name="oldPassword" id="oldPassword">
                </div>
                <div class="form-group row">
                    <label for="newPassord">New Password:</label>
                    <input type="password" class="form-control" name="newPassword" id="newPassword">
                </div>
                <div class="form-group row">
                    <label for="newPassword2">New Password Again:</label>
                    <input type="password" class="form-control" name="newPassword2" id="newPassword2">
                </div>

                <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
        </div>
    </body>
</html>
