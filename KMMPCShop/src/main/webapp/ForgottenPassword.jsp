<%-- 
    Document   : newjsp
    Created on : 2020.09.11., 17:03:43
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title>Forgotten password</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/navbar.jsp" %>
        <div class="container">
            <form method="POST" action="ForgottenPassword">
                <div class="form-group row">
                    <label for="email">Your email:</label>
                    <input type="email" class="form-control" name="email" id="email">
                </div>

                <button type="submit" class="btn btn-primary">Send new password</button>
            </form>
            <div class="float-right"><a href="${pageContext.request.contextPath}/Login.jsp">Back to login</a></div>
        </div>
    </body>
</html>
