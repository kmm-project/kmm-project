<%-- 
    Document   : UserRegistry
    Created on : 2020.08.17., 10:20:09
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div><%@include file="/background.jsp" %></div>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title> 
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>  
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <script>
            function validateForm() {
                var x = document.forms["registryForm"]["password"].value;
                var y = document.forms["registryForm"]["password2"].value;
                if (x !== y) {
                    alert("Not the same password");
                    return false;
                }
            }
        </script>
        <script>
            $(document).ready(function () {
                var date_input = $('input[name="date"]');
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy/mm/dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };
                date_input.datepicker(options);
            });
        </script>
        <script>
            function showPasswords() {
                var x = document.getElementById("password");
                var y = document.getElementById("password2");
                if (x.type === "password") {
                    x.type = "text";
                    y.type = "text";
                } else {
                    x.type = "password";
                    y.type = "password";
                }
            }
        </script>
    </head>
    <body>
        <%@ include file="/WEB-INF/navbar.jsp" %>
    <center><h2 style="color:red">${registryErrorMessage}</h2></center>
        <div class="container">

            <form name="registryForm" onsubmit="return validateForm()" method="POST"  action="UserRegistryServlet">
                <!-- <div class="form-group" required="">
                     <input type="radio" id="admin" name="role" value="admin" required=""> 
                     <label for="admin">Admin</label>
                     <input type="radio" id="user" name="role" value="user">
                     <label for="user">User</label>
                 </div>-->
                <div class="form-group row">
                    <label for="email">Email:</label>

                    <input type="email" class="form-control" placeholder="Email" id="email" name="email" required="">

                </div>
                <div class="form-group row">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="">
                    <input type="checkbox" onclick="showPasswords()">Show Passwords
                </div>
                <div class="form-group row">
                    <label for="password2">Password again:</label>
                    <input type="password" class="form-control" placeholder="Password" name="password2" id="password2" required="">

                </div>
                <div class="form-group row">
                    <label for="phone">Phone:</label>
                    <input type="text" class="form-control" placeholder="Phone" name="phone" id="phone">
                </div>
                <div class="form-group row">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" placeholder="Name" name="name" id="name">
                </div>
                <div class="form-group row">
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" placeholder="Address" name="address" id="address">
                </div>
                <div class="form-group row"> 
                    <label class="control-label" for="date">Date of birth:</label>
                    <input class="form-control" id="date" name="date" placeholder="YYYY/MM/DD" type="text"/>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </body>
</html>
