
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title>Login</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/navbar.jsp" %>
        <div><%@include file="/background.jsp" %></div>
        <center><h2 style="color:red">${loginErrorMessage}</h2></center>
        <form method="POST" action="Login">
            
            <div class="form-group">
                <label for="email">Email:</label>

                <div class="form-group">
                    <label for="email">Email:</label>

                    <input type="email" class="form-control" placeholder="Email" id="email" name="email" required="">

                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
            </form>
            <div class="float-right"><a href="${pageContext.request.contextPath}/ForgottenPassword.jsp">Forgot my password</a></div>
        </div>
    </body>
</html>
