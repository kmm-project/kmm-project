
package com.mycompany.kmmpcshop.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import com.mycompany.kmmpcshop.dto.ProductDTO;
import com.mycompany.kmmpcshop.entity.ProductEntity;


@Mapper
public interface ProductMapper
{
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductDTO toDTO(ProductEntity productEntity);

    List<ProductDTO> toDTOList(List<ProductEntity> productEntities);
	
	ProductEntity toEntity(ProductDTO productDTO);
}

