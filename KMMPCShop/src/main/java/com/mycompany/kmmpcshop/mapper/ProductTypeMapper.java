
package com.mycompany.kmmpcshop.mapper;

import com.mycompany.kmmpcshop.dto.ProductTypeDTO;
import com.mycompany.kmmpcshop.entity.ProductTypeEntity;
import java.util.ArrayList;
import java.util.List;


public class ProductTypeMapper {

    ProductTypeMapper INSTANCE;

    public static List<ProductTypeDTO> toDTOList(List<ProductTypeEntity> productTypeEntity) {

        List<ProductTypeDTO> productTypes = new ArrayList();

        for (int i = 0; i < productTypeEntity.size(); i++) {
            
            ProductTypeDTO productType = new ProductTypeDTO();
            
            productType.setName(productTypeEntity.get(i).getName());
            productType.setId(productTypeEntity.get(i).getId());
            productType.setProducts(ProductMapper.INSTANCE.toDTOList(productTypeEntity.get(i).getProductEntity()));

            productTypes.add(productType);
        }

        return productTypes;
    }
    
}
