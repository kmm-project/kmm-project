/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.mapper;

import com.mycompany.kmmpcshop.dto.FilterDTO;
import com.mycompany.kmmpcshop.entity.FilterEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author MMD
 */
@Mapper
public interface FilterMapper {

    FilterMapper INSTANCE = Mappers.getMapper(FilterMapper.class);

    FilterDTO toDTO(FilterEntity FilterEntity);

    List<FilterDTO> toDTOList(List<FilterEntity> FilterEntities);
}
