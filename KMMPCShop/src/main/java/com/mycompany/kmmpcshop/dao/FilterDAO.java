/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.dao;

import com.mycompany.kmmpcshop.entity.FilterEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author MMD
 */
@Singleton
public class FilterDAO {

    private static final String QUERY_FIND_ALL_FILTER = "select b from FilterEntity b ";

    @PersistenceContext
    private EntityManager em;

    public List<FilterEntity> findAll() {
        TypedQuery<FilterEntity> namedQuery = em.createNamedQuery(FilterEntity.QUERY_FIND_ALL_FILTER, FilterEntity.class);
        return namedQuery.getResultList();
    }
}
