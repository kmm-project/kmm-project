package com.mycompany.kmmpcshop.dao;

import com.mycompany.kmmpcshop.entity.UserEntity;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Singleton
public class UserDAO {

    @PersistenceContext
    private EntityManager em;

    public void addUser(UserEntity user) {
        em.persist(user);

    }

    public void UpdateUser(UserEntity user) {
        UserEntity updatedUser=emailQuery(user.getEmail());
        em.getTransaction().begin();
        updatedUser.setAddress(user.getAddress());
        updatedUser.setDob(user.getDob());
        updatedUser.setName(user.getName());
        updatedUser.setPhone(user.getPhone());
        em.getTransaction().commit();
    }

    public void setNewPassword(String email, String password) {
        UserEntity user = emailQuery(email);
        em.getTransaction().begin();
        user.setPassword(password);
        em.getTransaction().commit();
    }

    public String loginCheck(String email, String password) {
        UserEntity user = emailQuery(email);
        return user.getPassword();
    }

    public boolean cleanEmailCheck(String email) {
        UserEntity user = emailQuery(email);
        return user == null;
    }

    public UserEntity emailQuery(String email) {
        UserEntity user = null;
        try {
            TypedQuery<UserEntity> query = em.createNamedQuery("UserEntity.findByEmail", UserEntity.class).setParameter("email", email);
            user = query.getSingleResult();
        } catch (Exception e) {
        }
        return user;
    }

    public void forgottenPassword(String email, String password) {
        UserEntity user = emailQuery(email);
        em.getTransaction().begin();
        user.setPassword(password);
        em.getTransaction().commit();
    }
    public void removeUser(String email){
        UserEntity user = emailQuery(email);
        em.remove(user);
    }
}
