/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.service;

import com.mycompany.kmmpcshop.dao.UserDAO;
import com.mycompany.kmmpcshop.dto.UserDTO;
import com.mycompany.kmmpcshop.mapper.UserMapper;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class UserService {

    @Inject
    UserDAO userDAO;
    
    public void addUser(UserDTO user) throws NoSuchAlgorithmException, InvalidKeySpecException {
        user.setPassword(toEncryptPassword.passwordEncryption(user.getPassword()));
        userDAO.addUser(UserMapper.INSTANCE.toEntity(user));

    }

    public boolean registryCheck(String email, String password, String password2) {
        boolean emailRegex = Pattern.matches("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}", email);
        boolean passwordRegex = true;//passwordRegexCheck(password);                  //egyelőre csak a könnyebbségért
        return password.equals(password2) && password.length() >= 8 && passwordRegex && emailRegex && emailClean(email);
    }
    
    public boolean emailClean(String email){
        return userDAO.cleanEmailCheck(email);
    }

    public boolean loginCheck(String email, String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String storedPassword = userDAO.loginCheck(email, password);
        return toEncryptPassword.validatePassword(password, storedPassword);
    }

    public LocalDate dateCheckAndConverter(String date) {
        if(!date.isEmpty()){
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate dt = LocalDate.parse(date, dtf);
        return dt;}
        else{
            return null;
        }
    }
    
    public String localDateToString(LocalDate date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        return date.format(formatter);
    }

    public boolean passwordRegexCheck(String password) {
        return Pattern.matches(".*[a-z].*", password) && Pattern.matches(".*[A-Z].*", password) && Pattern.matches(".*[0-9].*", password) && Pattern.matches(".*[^A-Za-z0-9_].*", password);
    }

    public String forgottenPassword(String email) throws NoSuchAlgorithmException, InvalidKeySpecException {
        final int passwordLength = 10;
        String password = randomString(passwordLength);
        userDAO.forgottenPassword(email, toEncryptPassword.passwordEncryption(password));
        return password;
    }

    public static String randomString(int length) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz"
                + "!#$%&()*+,-./:;<=>?@[]^_{|}~";
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
    
    public void updateUser(UserDTO user) throws InvalidKeySpecException, NoSuchAlgorithmException{
        userDAO.UpdateUser(UserMapper.INSTANCE.toEntity(user));
    }
    
    public boolean newPassword(String email, String oldPassword, String newPassword, String newPassword2) throws NoSuchAlgorithmException, InvalidKeySpecException{
        boolean passwordRegex = true;//passwordRegexCheck(newPassword);
        String storedPassword=userDAO.loginCheck(email, oldPassword);
        if(toEncryptPassword.validatePassword(oldPassword, storedPassword) && passwordRegex && newPassword.equals(newPassword2) && newPassword.length() >= 8){
            userDAO.setNewPassword(email, toEncryptPassword.passwordEncryption(newPassword));
            return true;
        }
        return false;
    }
    
    public UserDTO getUser(String email){
        return UserMapper.INSTANCE.toDTO(userDAO.emailQuery(email));
    }

    public boolean UserLoggedIn(String email){
        return email!=null && !email.equals("")&& this.getUser(email)!=null;
    }
}
