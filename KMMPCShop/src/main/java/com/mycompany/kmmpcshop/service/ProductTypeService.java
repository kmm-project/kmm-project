/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.service;

import com.mycompany.kmmpcshop.dao.ProductTypeDAO;
import com.mycompany.kmmpcshop.entity.ProductTypeEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

/**
 *
 * @author MMD
 */
@Singleton
@TransactionAttribute
public class ProductTypeService {

    @Inject
    private ProductTypeDAO productTypeDAO;

    public List<ProductTypeEntity> findAll() {
        return productTypeDAO.findAll();
    }
}
