/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.servlet;

import com.mycompany.kmmpcshop.Enum.UserEnum;
import com.mycompany.kmmpcshop.dto.UserDTO;
import com.mycompany.kmmpcshop.service.UserService;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author User
 */
@WebServlet(name = "UserRegistryServlet", urlPatterns = {"/UserRegistryServlet"})
public class UserRegistryServlet extends HttpServlet {

    @Inject
    UserService service;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String password2 = req.getParameter("password2");
        LocalDate dob=service.dateCheckAndConverter(req.getParameter("date"));
        if (service.registryCheck(email, password, password2)) {
            UserDTO dto=new UserDTO();
            dto.setEmail(email);
            dto.setName(req.getParameter("name"));
            dto.setAddress(req.getParameter("address"));
            dto.setPhone(req.getParameter("phone"));
            dto.setRole(UserEnum.USER);
            dto.setPassword(password);
            dto.setDob(dob);
            try {
                service.addUser(dto);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                Logger.getLogger(UserRegistryServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            resp.sendRedirect(req.getContextPath()+"/Login");
        } else {
            String registryErrorMessage;
            if(!service.emailClean(email)){
                registryErrorMessage="The email is already in use";
            }else{
                registryErrorMessage="Passwords are not valid";
            }
            req.setAttribute("registryErrorMessage", registryErrorMessage);
            req.getRequestDispatcher("/UserRegistry.jsp").forward(req, resp);
            
        }


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/UserRegistry.jsp").forward(req, resp);
    }

}
