package com.mycompany.kmmpcshop.servlet;

import com.mycompany.kmmpcshop.bean.ListProductsPageBean;
import com.mycompany.kmmpcshop.entity.ProductEntity;
import com.mycompany.kmmpcshop.entity.ProductTypeEntity;
import com.mycompany.kmmpcshop.mapper.ProductMapper;
import com.mycompany.kmmpcshop.mapper.ProductTypeMapper;
import com.mycompany.kmmpcshop.service.ProductService;
import com.mycompany.kmmpcshop.service.ProductTypeService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/listProducts")
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "customer"}))
public class ListProductsServlet extends HttpServlet {
    
    private static final String LIST_PRODUCTS_PAGE_BEAN = "listProductsPageBean";
    
    @Inject
    private ProductService productService;
    
    @Inject
    private ProductTypeService productTypeService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<ProductEntity> products = productService.findAll();
        List<ProductTypeEntity> ProductTypes = productTypeService.findAll();
        
        ListProductsPageBean listProductsPageBean = (ListProductsPageBean) req.getSession()
                .getAttribute(LIST_PRODUCTS_PAGE_BEAN);
        
        if (listProductsPageBean == null) {
            listProductsPageBean = new ListProductsPageBean();
            req.getSession().setAttribute(LIST_PRODUCTS_PAGE_BEAN, listProductsPageBean);
        }
        
        listProductsPageBean.setProducts(ProductMapper.INSTANCE.toDTOList(products));
        
        listProductsPageBean.setProductTypes(ProductTypeMapper.toDTOList(ProductTypes));
        
        req.getRequestDispatcher("WEB-INF/listProducts.jsp").forward(req, resp);
        
    }
    
        @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        session.setAttribute("addFilter", Boolean.TRUE);
        
        String productIdString = request.getParameter("addFilter");
        Long productId = Long.parseLong(productIdString);
        
        ListProductsPageBean listProductsPageBean = (ListProductsPageBean) request.getSession()
                .getAttribute(LIST_PRODUCTS_PAGE_BEAN);
        
        if (listProductsPageBean == null) {
            listProductsPageBean = new ListProductsPageBean();
            request.getSession().setAttribute(LIST_PRODUCTS_PAGE_BEAN, listProductsPageBean);
        }
        
        listProductsPageBean.addProductIdForFilters(productId);

    }
}
