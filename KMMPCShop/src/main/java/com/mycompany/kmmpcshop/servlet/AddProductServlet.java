/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.servlet;

import com.mycompany.kmmpcshop.bean.ListProductsPageBean;
import com.mycompany.kmmpcshop.dto.ProductDTO;
import com.mycompany.kmmpcshop.entity.ProductEntity;
import com.mycompany.kmmpcshop.entity.ProductTypeEntity;
import com.mycompany.kmmpcshop.mapper.ProductMapper;
import com.mycompany.kmmpcshop.mapper.ProductTypeMapper;
import com.mycompany.kmmpcshop.service.AddingProduct;
import com.mycompany.kmmpcshop.service.ProductService;
import com.mycompany.kmmpcshop.service.ProductTypeService;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "AdminPage", urlPatterns = {"/AdminPage"})
public class AddProductServlet extends HttpServlet {
private static final String LIST_PRODUCTS_PAGE_BEAN = "listProductsPageBean";

    @Inject
    private AddingProduct addProductService;
    @Inject
    private ProductService productService;
    @Inject
    private ProductTypeService productTypeService;
 
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
ListProductsPageBean listProductsPageBean = (ListProductsPageBean) req.getSession()
                .getAttribute(LIST_PRODUCTS_PAGE_BEAN);

        if (listProductsPageBean == null) {
            listProductsPageBean = new ListProductsPageBean();
            req.getSession().setAttribute(LIST_PRODUCTS_PAGE_BEAN, listProductsPageBean);
        }

        List<ProductEntity> filteredProducts = productService.findFiltered(listProductsPageBean.getProductIdsForFilters());
        List<ProductEntity> products = productService.findAll();
        List<ProductTypeEntity> ProductTypes = productTypeService.findAll();

        listProductsPageBean.setFilteredProducts(ProductMapper.INSTANCE.toDTOList(filteredProducts));

        listProductsPageBean.setProducts(ProductMapper.INSTANCE.toDTOList(products));

        listProductsPageBean.setProductTypes(ProductTypeMapper.toDTOList(ProductTypes));
        
    req.getRequestDispatcher("/WEB-INF/AdminPage.jsp").forward(req, resp);

}
@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
        String description = request.getParameter("description");
        String manufacturer = request.getParameter("manufacturer");
        String name = request.getParameter("name");
        String priceString = request.getParameter("price");
        Integer price = Integer.parseInt(priceString);
//        String productTypeIdString = request.getParameter("productType_id");
        
        
        addProductService.addNewProduct(description, manufacturer, name, price);
response.sendRedirect(request.getContextPath() + "/AdminPage");


        
    }
}
