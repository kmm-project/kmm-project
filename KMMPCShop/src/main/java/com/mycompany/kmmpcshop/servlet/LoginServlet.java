/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.servlet;

import com.mycompany.kmmpcshop.dto.UserDTO;
import com.mycompany.kmmpcshop.service.UserService;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author User
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/Login"})
public class LoginServlet extends HttpServlet {
    @Inject
    UserService service;

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("Login.jsp").forward(request, response);
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session=request.getSession();
        String email=request.getParameter("email");
        String password=request.getParameter("password");
        session.setAttribute("email", email);
        boolean loginOK=false;
        try {
            loginOK = service.loginCheck(email, password);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(loginOK){
            UserDTO dto=service.getUser((String)session.getAttribute("email"));
            String name=dto.getName();
            String phone=dto.getPhone();
            String address=dto.getAddress();
            String dob="";
            if(dto.getDob()!=null){
            dob=service.localDateToString(dto.getDob());
            }
            session.setAttribute("name",name);
            session.setAttribute("phone", phone);
            session.setAttribute("address", address);
            session.setAttribute("dob", dob);
            session.setAttribute("loggedIn", true);
            response.sendRedirect(request.getContextPath()+"/index.jsp");
        }else{
            String loginErrorMessage="Wrong email or password";
            session.setAttribute("loginErrorMessage", loginErrorMessage);
            request.getRequestDispatcher("/Login.jsp").forward(request, response);
        }
        
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
