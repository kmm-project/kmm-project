/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.servlet;

import com.mycompany.kmmpcshop.service.JavaEmail;
import com.mycompany.kmmpcshop.service.UserService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author User
 */
@WebServlet(name = "ForgottenPassword", urlPatterns = {"/ForgottenPassword"})//még kell a jsp
public class ForgottenPassword extends HttpServlet {

    @Inject
    UserService service;

    private String host;
    private String port;
    private String user;
    private String pass;

    @Override
    public void init() {
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/ForgottenPassword").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String recipient = request.getParameter("email");
        if (service.emailClean(recipient)) {
            response.sendRedirect(request.getContextPath() + "/UserRegistry.jsp");
        } else {
            String subject = "Forgotten password";
            String content = "Your new password: ";
            try {
                content = content + service.forgottenPassword(recipient);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                Logger.getLogger(ForgottenPassword.class.getName()).log(Level.SEVERE, null, ex);
            }

            String resultMessage = "";

            try {

                JavaEmail.sendEmail(host, port, user, pass, recipient, subject,
                        content, false);
                resultMessage = "The email was sent successfully";
            } catch (UnsupportedEncodingException | MessagingException ex) {
                ex.printStackTrace();
                resultMessage = "There was an error: " + ex.getMessage();
            } finally {
                request.setAttribute("Message", resultMessage);
                response.sendRedirect(request.getContextPath() + "/Login.jsp");
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
