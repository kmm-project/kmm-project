/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.servlet;

import com.mycompany.kmmpcshop.Enum.UserEnum;
import com.mycompany.kmmpcshop.dto.UserDTO;
import com.mycompany.kmmpcshop.service.UserService;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author User
 */
@WebServlet(name = "UpdateUserServlet", urlPatterns = {"/UpdateUserServlet"})
public class UpdateUserServlet extends HttpServlet {
    @Inject
    UserService service;
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session=request.getSession(false);
        if((boolean)session.getAttribute("loggedIn")==true){
            request.getRequestDispatcher("/WEB-INF/UserSettings.jsp").forward(request, response);
//            response.sendRedirect(request.getContextPath()+"/UserSettings.jsp");
        }else{
            request.getRequestDispatcher(request.getContextPath()+"index.jsp").forward(request, response);
        }
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDTO dto=new UserDTO();
        HttpSession session=request.getSession(false);
        String email=(String)session.getAttribute("email");
        String name=request.getParameter("name");
        if(name.equals("")){
            name=(String)session.getAttribute("name");
        }
        String address=request.getParameter("address");
        if(address.equals("")){
            address=(String)session.getAttribute("address");
        }
        String phone=request.getParameter("phone");
        if(phone.equals("")){
            phone=(String)session.getAttribute("phone");
        }
        String date=request.getParameter("date");
        if(date.equals("")){
            date=(String)session.getAttribute("dob");
        }
        LocalDate dob=service.dateCheckAndConverter(date);
            dto.setEmail(email);
            dto.setPassword("");
            dto.setName(name);
            dto.setAddress(address);
            dto.setPhone(phone);
            dto.setRole(UserEnum.USER);
            dto.setDob(dob);
        try {
            service.updateUser(dto);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
            Logger.getLogger(UpdateUserServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect(request.getContextPath()+"/index.jsp");
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
