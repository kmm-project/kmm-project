package com.mycompany.kmmpcshop.entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author MMD
 */
@Entity
@Table(name = "activateFilters")
@Data
public class ActivateFilterEntity extends BaseEntity {

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "activatefilter_product_jt",
            joinColumns = @JoinColumn(name = "activatefilter_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<ProductEntity> products;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "activatefilter_filter_jt",
            joinColumns = @JoinColumn(name = "activatefilter_id"),
            inverseJoinColumns = @JoinColumn(name = "filter_id"))
    private List<FilterEntity> filters;
    
}
