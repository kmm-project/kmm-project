package com.mycompany.kmmpcshop.entity;

import static com.mycompany.kmmpcshop.entity.ProductEntity.QUERY_FIND_ALL;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "product")
@NamedQuery(name = QUERY_FIND_ALL, query = "select b from ProductEntity b")
public class ProductEntity extends BaseEntity {

    public static final String QUERY_FIND_ALL = "ProductEntity.findAll";

    @Column
    private String name;

    @Column
    private String manufacturer;

    @Column
    private String description;
	
	@Column
    private int price;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToMany(mappedBy = "products")
    private List<FilterEntity> filters;

    @ManyToMany(mappedBy = "products", fetch = FetchType.EAGER)
    private List<ActivateFilterEntity> activateFilters;

    @ManyToOne
    @JoinColumn(name = "productType_id")
    private ProductTypeEntity productTypeEntity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
	
	public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
	
    public List<FilterEntity> getFilters() {
        return filters;
    }

    public void setFilters(List<FilterEntity> filters) {
        this.filters = filters;
    }

    public ProductTypeEntity getProductTypeEntity() {
        return productTypeEntity;
    }

    public void setProductTypeEntity(ProductTypeEntity productTypeEntity) {
        this.productTypeEntity = productTypeEntity;
    }

    public List<ActivateFilterEntity> getActivateFilters() {
        return activateFilters;
    }

    public void setActivateFilters(List<ActivateFilterEntity> activateFilters) {
        this.activateFilters = activateFilters;
    }

}
