/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.entity;

import static com.mycompany.kmmpcshop.entity.ProductTypeEntity.QUERY_FIND_ALL;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author MMD
 */
@Entity
@Table(name = "ProductType")
@NamedQuery(name = QUERY_FIND_ALL, query = "select b from ProductTypeEntity b")
public class ProductTypeEntity extends BaseEntity {

    public static final String QUERY_FIND_ALL = "ProductTypeEntity.findAll";

    @Column
    private String name;

    @OneToMany(mappedBy = "productTypeEntity", fetch = FetchType.EAGER)
    private List<ProductEntity> productEntity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductEntity> getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(List<ProductEntity> productEntity) {
        this.productEntity = productEntity;
    }

}
