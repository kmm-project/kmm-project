/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.entity;

import com.mycompany.kmmpcshop.Enum.UserEnum;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;
import lombok.NonNull;
import lombok.NoArgsConstructor;
//import static com.mycompany.kmmpcshop.entity.UserEntity.QUERY_FIND_BY_EMAIL;

@NoArgsConstructor
@Data
@Entity
@Table(name="user")
@NamedQueries({
    @NamedQuery(name = "UserEntity.findByEmail", query = "select u from UserEntity u where u.email=:email"),
    @NamedQuery(name = "UserEntity.findAll", query = "select u from UserEntity u")
})
public class UserEntity extends BaseEntity implements Serializable {
    
//    public static final String QUERY_FIND_BY_EMAIL="UserEntity.findByEmail";
    @Column
    @NonNull
    private String email;
    @Column
    @NonNull
    private String password;
    @Column
    @NonNull
    @Enumerated(EnumType.STRING)
    private UserEnum userRole;
    @Column
    private String address;
    @Column
    private LocalDate dob;
    @Column
    private String phone;
    @Column
    private String name;
    
}
