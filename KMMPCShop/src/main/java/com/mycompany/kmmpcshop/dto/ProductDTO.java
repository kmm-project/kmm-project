package com.mycompany.kmmpcshop.dto;

public class ProductDTO extends BaseDTO {

    private String name;
    private String manufacturer;
    private String description;
    private ActivateFilterDTO activateFilter;
    private ProductTypeDTO productType;
	private int price;
	
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
	
	public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
	
    public ActivateFilterDTO getActivateFilter() {
        return activateFilter;
    }

    public void setActivateFilter(ActivateFilterDTO activateFilter) {
        this.activateFilter = activateFilter;
    }

    public ProductTypeDTO getProductType() {
        return productType;
    }

    public void setProductType(ProductTypeDTO productTypeEntity) {
        this.productType = productTypeEntity;
    }

    @Override
    public String toString() {
        return "ProductDTO{"
                + "id='" + getId() + '\''
                + "name='" + name + '\''
                + ", manufacturer='" + manufacturer + '\''
                + ", description='" + description + '\''
                + '}';
    }
    
    
}
