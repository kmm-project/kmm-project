package com.mycompany.kmmpcshop.bean;

import com.mycompany.kmmpcshop.dto.ProductDTO;
import com.mycompany.kmmpcshop.dto.ProductTypeDTO;
import java.util.ArrayList;
import java.util.List;

public class ListProductsPageBean {

    List<ProductDTO> products;
    List<ProductDTO> filteredProducts;
    List<ProductDTO> selectedProducts;
    List<ProductTypeDTO> productTypes;
    List<Long> productIdsForFilters;

    public ListProductsPageBean() {
        this.products = new ArrayList<ProductDTO>();
        this.productIdsForFilters = new ArrayList<Long>();
        this.selectedProducts = new ArrayList<ProductDTO>();
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public List<ProductTypeDTO> getProductTypes() {

        return productTypes;
    }

    public void setProductTypes(List<ProductTypeDTO> productTypes) {

        this.productTypes = productTypes;
    }

    public List<Long> getProductIdsForFilters() {
        return productIdsForFilters;
    }
    
    public void clearProductIdsForFilters() {
        this.productIdsForFilters.clear();
    }

    public void setProductIdsForFilters(List<Long> productIdsForFilters) {
        this.productIdsForFilters = productIdsForFilters;
    }

    public void addProductIdForFilters(Long id) {
        this.productIdsForFilters.add(id);
    }
    
    public void addToselectedProducts(ProductDTO pdto) {
        this.selectedProducts.add(pdto);
    }

    public List<ProductDTO> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(List<ProductDTO> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }
    
    public static ProductDTO getProductById(List<ProductDTO> products, Long id) {
        
        ProductDTO product = new ProductDTO();
        
        for (int i = 0; i < products.size(); i++) {
            if(products.get(i).getId() == id){
                product = products.get(i);
            }
        }
        return product;
    }
    
    public static void removeProductById(List<ProductDTO> products, Long id) {
        
        ProductDTO product = new ProductDTO();
        
        for (int i = 0; i < products.size(); i++) {
            if(products.get(i).getId() == id){
                products.remove(i);
            }
        }
    }
    
    public static void removeId(List<Long> ids, Long id) {
        
        for (int i = 0; i < ids.size(); i++) {
            if(ids.get(i) == id){
                ids.remove(i);
            }
        }
    }

    
    
    
    public void clearSelectedProducts() {
        this.selectedProducts.clear();
    }

    

    public List<ProductDTO> getFilteredProducts() {
        return filteredProducts;
    }
    
    public void clearFilteredProducts() {
        this.filteredProducts.clear();
    }

    public void setFilteredProducts(List<ProductDTO> filteredProducts) {
        this.filteredProducts = filteredProducts;
    }

}
